package br.com.testes.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.testes.dao.PessoaDAO;
import br.com.testes.dto.PessoaDTO;
import br.com.testes.entity.PessoaEntity;

@Service
public class PessoaService {

	@Autowired
	private PessoaDAO pessoaDAO;
	
	public List<PessoaDTO> findAll(){
		List<PessoaDTO> listDTO = new ArrayList<PessoaDTO>();
		List<PessoaEntity> listEnitty = pessoaDAO.findAll();
		for(PessoaEntity entity : listEnitty) {
			PessoaDTO dto = PessoaDTO.parse(entity);
			listDTO.add(dto);
		}
		return listDTO;
	}
	
	@Transactional
	public void save(PessoaDTO dto) {
		PessoaEntity entity = PessoaEntity.parse(dto);
		entity.setDataInclusao(new Date());
		pessoaDAO.persist(entity);
	}
	
	@Transactional
	public void update(PessoaDTO dto) {
		PessoaEntity entity = PessoaEntity.parse(dto);
		pessoaDAO.update(entity);
	}
	
	@Transactional
	public void delete(Integer id) {
		pessoaDAO.delete(id);
	}

}
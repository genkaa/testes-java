package br.com.testes.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.testes.dao.CidadeDAO;
import br.com.testes.dto.CidadeDTO;
import br.com.testes.entity.CidadeEntity;

@Service
public class CidadeService {

	@Autowired
	private CidadeDAO cidadeDAO;
	
	public List<CidadeDTO> findAll(){
		List<CidadeDTO> listDTO = new ArrayList<CidadeDTO>();
		List<CidadeEntity> listEnitty = cidadeDAO.findAll();
		for(CidadeEntity entity : listEnitty) {
			CidadeDTO dto = new CidadeDTO();
			dto.setId(entity.getId());
			dto.setNome(entity.getNome());
			listDTO.add(dto);
		}
		return listDTO;
	}
}
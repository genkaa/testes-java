package br.com.testes.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import br.com.testes.dto.PessoaDTO;

@Entity
@Table(name = "PESSOA")
public class PessoaEntity {

	@Id
	@Column(name = "ID")
	@GeneratedValue(strategy = GenerationType.SEQUENCE)
	private Integer id;

	@Column(name = "NOME")
	private String nome;

	@Column(name = "IDADE")
	private Integer idade;

	@Column(name = "DATA_INCLUSAO")
	private Date dataInclusao;

	@ManyToOne
	@JoinColumn(name="ID_CIDADE")
	private CidadeEntity cidade;

	public Integer getId() {
		return id;
	}

	public String getNome() {
		return nome;
	}

	public Integer getIdade() {
		return idade;
	}

	public Date getDataInclusao() {
		return dataInclusao;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public void setIdade(Integer idade) {
		this.idade = idade;
	}

	public void setDataInclusao(Date dataInclusao) {
		this.dataInclusao = dataInclusao;
	}

	public CidadeEntity getCidade() {
		return cidade;
	}

	public void setCidade(CidadeEntity cidade) {
		this.cidade = cidade;
	}

	@Override
	public String toString() {
		return "TesteEntity [id=" + id + ", nome=" + nome + ", idade=" + idade + ", dataInclusao=" + dataInclusao
				+ ", cidade=" + cidade.getNome() + "]";
	}

	public static PessoaEntity parse(PessoaDTO dto) {
		PessoaEntity entity = new PessoaEntity();
		entity.setId(dto.getId());
		entity.setIdade(dto.getIdade());
		entity.setNome(dto.getNome());
		entity.setCidade(CidadeEntity.parse(dto.getCidadeDTO()));
		return entity;
	}
	
}

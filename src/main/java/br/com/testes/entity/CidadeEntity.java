package br.com.testes.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import br.com.testes.dto.CidadeDTO;

@Entity
@Table(name="CIDADE")
public class CidadeEntity {

	public CidadeEntity() {
	}
	
	public CidadeEntity(Integer id){
		this.id = id;
	}
	
	@Id
	@Column(name = "ID")
	private Integer id;

	@Column(name = "NOME")
	private String nome;

	public Integer getId() {
		return id;
	}

	public String getNome() {
		return nome;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public static CidadeEntity parse(CidadeDTO dto) {
		CidadeEntity entity = new CidadeEntity();
		entity.setId(dto.getId());
		entity.setNome(dto.getNome());
		return entity;
	}
	
	
}

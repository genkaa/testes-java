package br.com.testes.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;

import org.springframework.stereotype.Repository;

import br.com.testes.entity.CidadeEntity;

@Repository
public class CidadeDAO {
	@PersistenceContext
	private EntityManager entityManager;

	public List<CidadeEntity> findAll() {
		CriteriaBuilder builder = entityManager.getCriteriaBuilder();
		CriteriaQuery<CidadeEntity> criteria = builder.createQuery(CidadeEntity.class);
		criteria.from(CidadeEntity.class);
		return entityManager.createQuery(criteria).getResultList();
	}

}
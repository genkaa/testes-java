package br.com.testes.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import org.springframework.stereotype.Repository;

import br.com.testes.entity.PessoaEntity;

@Repository
public class PessoaDAO {
	@PersistenceContext
	private EntityManager entityManager;

	public List<PessoaEntity> findAll() {
		TypedQuery<PessoaEntity> query = entityManager.createQuery("FROM PessoaEntity", PessoaEntity.class);
		return query.getResultList();
	}
	
	public PessoaEntity persist(PessoaEntity entity) {
		entityManager.persist(entity);
		return entity;
	}
	
	public PessoaEntity update(PessoaEntity entity) {
		entityManager.merge(entity);
		return entity;
	}
	
	public PessoaEntity delete(Integer id) {
		PessoaEntity pessoaEntity = entityManager.find(PessoaEntity.class, id);
		entityManager.remove(pessoaEntity);
		return pessoaEntity;
	}
}
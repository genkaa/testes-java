package br.com.testes.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.servlet.ModelAndView;

import br.com.testes.dto.CidadeDTO;
import br.com.testes.dto.PessoaDTO;
import br.com.testes.service.CidadeService;
import br.com.testes.service.PessoaService;

@Controller
@RequestMapping(value = "/pessoa")
public class PessoaController {

	@Autowired
	private PessoaService pessoaService;
	
	@Autowired
	private CidadeService cidadeService;
	
	@GetMapping("/")
	public ModelAndView init(HttpServletRequest req, HttpServletResponse resp) {
		List<CidadeDTO> cidades = cidadeService.findAll();
		
		ModelAndView modelAndView = new ModelAndView("index");
	    modelAndView.addObject("cidades", cidades);
	    
		return modelAndView;
	}
	
	@ResponseStatus(value = HttpStatus.OK)
	@GetMapping(path="/findAll", produces="application/json; charset=UTF-8")
	public List<PessoaDTO> findAll() {
		List<PessoaDTO> list = pessoaService.findAll();
		return list;
	}
	
	@ResponseStatus(value = HttpStatus.OK)
	@PostMapping(path="/save", consumes="application/json; charset=UTF-8")
	public void save(@RequestBody PessoaDTO dto) {
		pessoaService.save(dto);
	}
	
	@ResponseStatus(value = HttpStatus.OK)
	@PutMapping(path="/update", consumes="application/json; charset=UTF-8")
	public void update(@RequestBody PessoaDTO dto) {
		pessoaService.update(dto);
	}
	
	@ResponseStatus(value = HttpStatus.OK)
	@DeleteMapping(path="/delete", consumes="application/json; charset=UTF-8")
	public void delete(@RequestBody Integer id) {
		pessoaService.delete(id);
	}
}

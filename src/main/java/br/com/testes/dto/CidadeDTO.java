package br.com.testes.dto;

import br.com.testes.entity.CidadeEntity;

public class CidadeDTO {

	private Integer id;

	private String nome;

	public Integer getId() {
		return id;
	}

	public String getNome() {
		return nome;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}
	
	
	public static CidadeDTO parse(CidadeEntity entity) {
		CidadeDTO dto = new CidadeDTO();
		dto.setId(entity.getId());
		dto.setNome(entity.getNome());
		return dto;
	}

}

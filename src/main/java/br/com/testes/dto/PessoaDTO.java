package br.com.testes.dto;

import java.util.Date;

import br.com.testes.entity.PessoaEntity;

public class PessoaDTO {

	private Integer id;

	private String nome;

	private Integer idade;

	private Date dataInclusao;

	private CidadeDTO cidadeDTO;

	public Integer getId() {
		return id;
	}

	public String getNome() {
		return nome;
	}

	public Integer getIdade() {
		return idade;
	}

	public Date getDataInclusao() {
		return dataInclusao;
	}

	public CidadeDTO getCidadeDTO() {
		return cidadeDTO;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public void setIdade(Integer idade) {
		this.idade = idade;
	}

	public void setDataInclusao(Date dataInclusao) {
		this.dataInclusao = dataInclusao;
	}

	public void setCidadeDTO(CidadeDTO cidadeDTO) {
		this.cidadeDTO = cidadeDTO;
	}
	
	public static PessoaDTO parse(PessoaEntity entity) {
		PessoaDTO dto = new PessoaDTO();
		dto.setDataInclusao(entity.getDataInclusao());
		dto.setId(entity.getId());
		dto.setIdade(entity.getIdade());
		dto.setNome(entity.getNome());
		dto.setCidadeDTO(CidadeDTO.parse(entity.getCidade()));
		return dto;
	}
	
}

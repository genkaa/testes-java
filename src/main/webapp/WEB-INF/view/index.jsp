<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
<script>
	$(document).ready(function() {
		findAll = function(){
			$.ajax({
				url:'findAll/',
				method:'GET',
				async:false,
				success:function(obj, status, jqXHR){
					$("#pessoasTable tbody").html("");
					obj.pessoaDTOList.forEach(function(item){
						var btnUpdate = "<input type='button' id='update_" + item.id + "' onclick='updatePessoa(" + item.id + ")' value='update' />";
						var btnDelete = "<input type='button' id='delete_" + item.id + "' onclick='deletePessoa(" + item.id + ")' value='X'/>";
						var row = "<tr id='row_" + item.id + "'>";
						row += "<td>" + item.id + "</td>";
						row += "<td><input id='nomeUpdate' type='text' value='" + item.nome + "' /></td>";
						row += "<td><input id='idadeUpdate' type='text' value='" + item.idade + "' /></td>";
						row += "<td id='tdCidade_" + item.id + "'>";
						row += $("#selectCidades").get(0).outerHTML;
						row += "</td>";
						row += "<td>" + btnUpdate + "</td><td>" + btnDelete + "</td>";
						row += "</tr>";
						$("#pessoasTable tbody").append(row);
						$('#tdCidade_' + item.id).find("select option[id=" + item.cidadeDTO.id + "]").attr('selected', 'selected');
						
					});
				}
			});
		}
		findAll();
		$("#btn").on('click', function(){
			var cidade = new Object();
			cidade.id = $("#cadastro").find("#selectCidades option:selected").attr('id');

			var obj = new Object();
			obj.nome = $("#nome").val();
			obj.idade = $("#idade").val();
			obj.cidadeDTO = cidade;
			
			$.ajax({
				url:'save/',
				method:'POST',
				contentType: "application/json",
				data:JSON.stringify(obj),
				async:false,
				success:function(obj, status, jqXHR){
					findAll();
				}
			});
		});
		
		updatePessoa = function( idPessoa ){
			var cidade = new Object();
			cidade.id = $('#tdCidade_' + idPessoa).find("select option:selected").attr('id');
			
			var obj = new Object();
			obj.nome = $("#row_" + idPessoa).find("#nomeUpdate").val();
			obj.idade = $("#row_" + idPessoa).find("#idadeUpdate").val();
			obj.id=idPessoa;
			obj.cidadeDTO = cidade;
			
			$.ajax({
				url:'update/',
				method:'PUT',
				contentType: "application/json",
				data:JSON.stringify(obj),
				async:false,
				success:function(obj, status, jqXHR){
					findAll();
				}
			});
		}
		
		deletePessoa = function( idPessoa ){
			
			$.ajax({
				url:'delete/',
				method:'DELETE',
				contentType: "application/json",
				data:JSON.stringify(idPessoa),
				async:false,
				success:function(obj, status, jqXHR){
					findAll();
				}
			});
		}
	});
</script>

<table id="pessoasTable" border="1">
	<thead>
		<th>id</th>
		<th>nome</th>
		<th>idade</th>
		<th>cidade</th>
		<th>&nbsp;</th>
		<th>&nbsp;</th>
	</thead>
	<tbody>
	</tbody>
</table>

<br /><br /><br />
<div id="cadastro">
	<label for="nome">Nome:</label>
	<input type="text" id="nome" /><br/>
	
	<label for="idade">Idade:</label>
	<input type="text" id="idade" /><br/>
	
	<label for="selectCidades">Cidade:</label>
	<select id="selectCidades">
		<c:forEach items="${cidades}" var="cidade">
			<option name="cidades" id="${cidade.id}" >${ cidade.nome }</option>
		</c:forEach>
	</select>
	<br />
	<input id="btn" type="button" type="button" value="Cadastrar" />
</div>
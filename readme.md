Tecnologias:
	Banco H2
	JPA
	Spring Boot
	Spring MVC
	Maven
	Jquery

Download Banco H2
http://www.h2database.com/html/download.html

Ferramenta consultas H2
http://localhost:8082/login.jsp

DROP TABLE PESSOA;

DROP TABLE CIDADE;

DROP SEQUENCE PESSOA_SEQ;

CREATE TABLE 
	CIDADE (
		ID INT PRIMARY KEY,
		NOME VARCHAR(100)
	);
	
INSERT INTO CIDADE VALUES(1, 'SAO PAULO');

INSERT INTO CIDADE VALUES(2, 'SAO CAETANO');

CREATE SEQUENCE PESSOA_SEQ;

CREATE TABLE
    PESSOA(
        ID INTEGER DEFAULT (NEXT VALUE FOR PESSOA_SEQ) NOT NULL,
        NOME CHAR(100),
        IDADE INTEGER,
        DATA_INCLUSAO DATE,
        ID_CIDADE INTEGER,
        PRIMARY KEY (ID),
        CONSTRAINT TESTE_ID_CIDADE_FK FOREIGN KEY (ID_CIDADE) REFERENCES CIDADE (ID)
	);
	
INSERT INTO PESSOA VALUES(PUBLIC.PESSOA_SEQ.nextval, 'TESTE NOME', 30, '2019-01-10', 1);

executar classe br.com.testes.Application.java para subir o servidor

Tela cadastro Pessoa
http://localhost:9081/pessoa/